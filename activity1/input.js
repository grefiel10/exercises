import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, TextInput, View } from 'react-native';


class Inputs extends Component {

    state = {
        textInput: '',
    }
    textHandler = (text)=>{
        this.setState({ textInput: text })
    }

    submit = (textInput) => {

        alert('You just typed: ' + textInput)
    }
    render() {
        return(
            <View style = {styles.container}>
                <TextInput 
                    style = {styles.input}                  
                    onChangeText = {this.textHandler}
                />
    
            <TouchableOpacity
               style = {styles.click}
               onPress = {
                  () => this.submit(this.state.textInput)
               }>
                 <Text style = {styles.buttonText}> Click Me </Text>
            </TouchableOpacity>
            </View>
        )
    }

}
export default Inputs

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#26A69A',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
      borderWidth: 1,
      borderColor: '#ECEFF1',
      padding: 8,
      margin: 10,
      width: 200,
    },
    click: {
      padding: 10,
      borderRadius: 10,
      backgroundColor: '#448AFF'
    },
    buttonText: {
      color: '#FAFAFA',
    }
  });
  