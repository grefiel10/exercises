
import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#C39EA0',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5,
        paddingVertical: 10,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 19,
        height: 19,
        backgroundColor: '#C39EA0',
        borderColor: '#F8E5E5',
        borderWidth: 3,
        borderRadius: 60,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '85%',
        backgroundColor: '#C39EA0',
        color: '#000000',
        fontWeight: 'normal',
        fontSize: 15,
    },
});
export default Task;
